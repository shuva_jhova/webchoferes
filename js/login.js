function muestraReloj() {
			var fechaHora = new Date();
			var horas = fechaHora.getHours();
			var minutos = fechaHora.getMinutes();
			var segundos = fechaHora.getSeconds();
			if(horas < 10) { horas = '0' + horas; }
			if(minutos < 10) { minutos = '0' + minutos; }
			if(segundos < 10) { segundos = '0' + segundos; }
			document.getElementById("reloj").innerHTML = horas+':'+minutos+':'+segundos;
			}
function reloj() {

setInterval(muestraReloj, 1);
}

function login(){

	
	var user=document.getElementById("txtuser").value;
	var pass=document.getElementById("txtpassword").value;
	
		//localStorage.setItem('estacion',estacion);
		REQUEST('usersFunction', 'login', [user,pass], onLogin, onerror);
}


function onerror(errObject)
{
		alert(errObject["Message"] + ":\n" + errObject["Detail"]);
}


function onLogin(dataResult)
{
	if(dataResult.length>0)
	{	
		if(dataResult[0]['usuario'].localeCompare("en sesion")!=0){
			localStorage.setItem('id',dataResult[0]['idusuario']);
			localStorage.setItem('perfil',dataResult[0]['idperfil']);
			localStorage.setItem('operador',dataResult[0]['operador']);
			localStorage.setItem('nombre',dataResult[0]['nombre']);
			localStorage.setItem('usuario',dataResult[0]['usuario']);
			document.location.href = 'php/inicio.php';
		}
		else{
			alert('Este usuario tiene una sesion activa, por el momento no puede volver a ingresar. Intente nuevamente mas tarde.');
		}
	}else{
		alert('Nombre de usuario y/o clave de acceso incorrectos. Intente nuevamente.');
	
	}
}

function getestaciones(){
	
	REQUEST('appControl', 'getestacionesactivas', [] , onEstaciones, onerror);
}

function onEstaciones(dataResult){
	if(dataResult.length>0){
		var op=document.getElementById("optionestacion");		
		for (var a = 0; a<dataResult.length; a++)
		{	
			var option = document.createElement("option");
	    		option.value=dataResult[a]['id'];
    			option.text =dataResult[a]['nombre'];
			op.appendChild(option);
		}
	}
}