var str2;
function showEdit(editableObj) {
			editableObj.style.backgroundColor="#e3bfc4";

		}
function edita(editableObj) {
			editableObj.style.backgroundColor="#FFCC99";
		}

function onerror(errObject)
{
		alert(errObject["Message"] + ":\n" + errObject["Detail"]);
}


function saverecord(str)
{
	var nombre = document.getElementById('proceso'+str).innerText;
	var tiempo = document.getElementById('tiempo2'+str).value;
	var costo = document.getElementById('costo'+str).innerText;
	var doctor = document.getElementById('optiondoctor'+str).value;
	var activo = document.getElementById('optionactivo'+str).value;
	

 	if(nombre.length == 0)
	{
		alert("El nombre del proceso es invalido");
	}
	else
	 if(tiempo.length == 0 || tiempo.localeCompare("00:00")==0)
	{
		alert("El tiempo de duraci�n es invalido");
	}else
	 if(costo.length==0 || costo <=0 || !isNumber(costo))
	{
		alert("El costo es invalido");
	}
	else if(doctor.length==0)
	{
		alert("El doctor seleccionado es invalido");
	}
	else
	{
		REQUEST('appControl', 'updateproceso', [str,doctor,nombre,tiempo,costo,activo], onUpdateproceso, onerror);
	}
}

function onUpdateproceso(dataResult)
{
	
	if(dataResult[0]['exito'].localeCompare("si")==0)
	{	getprocess();
		alert("Exito al modificar los datos");
			
	}
	else if(dataResult[0]['exito'].localeCompare("sin")==0)
	{
		alert("El proceso no ha sido encontrado");
	}else
	{
		alert("Error al modificar los datos. Intente nuevamente");
	}
    	
	
}

function getdoctors(){
REQUEST('appControl', 'getalldoctors', [] , onDoctor, onerror);
}

function onDoctor(dataResult){
	var row=document.getElementById(str2);
	var cells = row.getElementsByTagName('td')
	var doctor=cells[1].innerText;
	var selectList = document.createElement("select");
   		selectList.id="optiondoctor"+str2;
 		
		

	var doc=document.getElementById("optiondoctor");
	if(dataResult.length>0){
		for(var a=0;a<dataResult.length;a++){
			var datos=dataResult[a]['nombre']+' '+dataResult[a]['appat']+' '+dataResult[a]['apmat'];
			if(datos.localeCompare(doctor)==0){
				var option = document.createElement("option");
	    			option.value=dataResult[a]['id'];
    			 	option.text = datos;
				option.selected = 'selected';
    			 	selectList.appendChild(option);
			}else{
				var option = document.createElement("option");
	    			option.value=dataResult[a]['id'];
    			 	option.text = datos;
				selectList.appendChild(option);
			}
		}	
	}else{
		var option = document.createElement("option");
    		option.value="sindocs";
    		option.text = "--SIN DOCTORES REGISTRADOS--";
    		doc.appendChild(option);		
	}

	cells[1].innerHTML="";
	cells[1].innerText="";
	cells[1].contentEditable="false";
	cells[1].appendChild(selectList);
}


function getprocess(){
	document.getElementById("imgload").style.display="block";
	REQUEST('appControl', 'getallprocess', [] , onProceso, onerror);
}

function onProceso(dataResult){
	document.getElementById("imgload").style.display="none";
	myDeleteFunction() ;
	for (var i = 0; i<dataResult.length; i++)
	{

			var tableRef = document.getElementById('resultado').getElementsByTagName('tbody')[0];;
			// Insert a row in the table at the last row
			var newRow   = tableRef.insertRow(tableRef.rows.length);
			newRow.className = "table-row";
			newRow.id=dataResult[i]["id"];
			// Insert a cell in the row at index 0
			var newCell  = newRow.insertCell(0);
			newCell.id="proceso"+dataResult[i]["id"];
			//newCell.contentEditable = "true";
			//newCell.addEventListener('click', showEdit.bind(null,newCell));
			// Append a text node to the cell
			var newText  = document.createTextNode(dataResult[i]["proceso"]);
			newCell.appendChild(newText);

			 newCell  = newRow.insertCell(1);
			newCell.id="doctor"+dataResult[i]["id"];
			//newCell.contentEditable = "true";	
			// Append a text node to the cell
			 newText  = document.createTextNode(dataResult[i]["nombre"]+" "+dataResult[i]["appat"]+" "+dataResult[i]["apmat"]);
			newCell.appendChild(newText);


 			newCell  = newRow.insertCell(2);
			newCell.id="tiempo"+dataResult[i]["id"];
			//newCell.contentEditable = "true";
			newText = document.createTextNode(dataResult[i]["tiempo"]);
			newCell.appendChild(newText);
			
			newCell  = newRow.insertCell(3);
			newCell.id="costo"+dataResult[i]["id"];
			//newCell.contentEditable = "true";
			// Append a text node to the cell
			newText  = document.createTextNode(dataResult[i]["costo"]);
			newCell.appendChild(newText);

			newCell  = newRow.insertCell(4);
			newCell.id="activo"+dataResult[i]["id"];
			//newCell.contentEditable = "true";
			// Append a text node to the cell
			var activo="";
			if(dataResult[i]["activo"]>0){
				activo="Si";
			}else{
				activo="No";
			}
			newText  = document.createTextNode(activo);
			newCell.appendChild(newText);

			newCell  = newRow.insertCell(5);
			var img = document.createElement('img');
   			img.src = "../img/editar.png";
			img.id="imgeditar"+dataResult[i]["id"];
			img.addEventListener('click', edit.bind(null,dataResult[i]["id"]));
			newCell.appendChild(img);
			var img3 = document.createElement('img');
   			 img3.src = "../img/save.png";
			img3.id="imgsave"+dataResult[i]["id"];
			img3.style.display="none";
			img3.addEventListener('click', saverecord.bind(null,dataResult[i]["id"]));
			newCell.appendChild(img3);
			var img4 = document.createElement('img');
   			img4.src = "../img/cancel.png";
			img4.id="imgcancel"+dataResult[i]["id"];
			img4.style.display="none";
			img4.addEventListener('click', cancela.bind(null,dataResult[i]["id"]));
			newCell.appendChild(img4);

			newCell  = newRow.insertCell(6);
			var img2 = document.createElement('img');
   			img2.src = "../img/delete.png";
			img2.addEventListener('click', deleterecord.bind(null,dataResult[i]["id"]));
			newCell.appendChild(img2);

			//newRow.addEventListener('click', navigateToController.bind(null, dataResult[i]["usuario"]));
	}	
}
function cancela(str){

	var si=confirm("Aunque los cambios hallan sido realizados no se guardaran\n�Seguro que desea cancelar la modificacion sin guardar?");
	if(si){ 
		var row=document.getElementById(str);
		row.className = "table-row";
		var cells = row.getElementsByTagName('td')
		for(var a=0;a<cells.length-2;a++){
			//if(a!=20){
				cells[a].contentEditable = "false";
				cells[a].style.backgroundColor="mediumseagreen";
				cells[a].addEventListener('focus', null);
			//}else if(a==20){
			//	cells[a].contentEditable = "false";
			//	cells[a].style.backgroundColor="mediumseagreen";
				//cells[a].onclick="";
			//	cells[a].removeEventListener('click', whatever);
			//}
		}

		var doctor=document.getElementById('optiondoctor'+str);
		//cells[3].innerHTML=sexo;
		cells[1].innerText=doctor.options[doctor.selectedIndex].text;

		var activo=document.getElementById('optionactivo'+str);
		//cells[3].innerHTML=sexo;
		cells[4].innerText=activo.options[activo.selectedIndex].text;;

		var tiempo=document.getElementById('tiempo2'+str).value;
		//cells[3].innerHTML=sexo;
		cells[2].innerText=tiempo;

	
		var img1=document.getElementById('imgeditar'+str);
		var img2=document.getElementById('imgsave'+str);
		var img3=document.getElementById('imgcancel'+str);
		img1.style.display="block";
		img2.style.display="none";
		img3.style.display="none";
	}
}

function edit(str){
	str2=str;
	var row=document.getElementById(str);
	row.className = "table-row2";
	var cells = row.getElementsByTagName('td')
	for(var a=0;a<cells.length-2;a++){
		cells[a].style.backgroundColor="#9adbcf";
		//if(a!=9 && a!=20){
		cells[a].contentEditable = "true";
		cells[a].addEventListener('focus', showEdit.bind(null,cells[a]));
		//}else if(a==20){
		//	cells[a].contentEditable = "true";
		//	cells[a].addEventListener('click', whatever=this.enferedit.bind(null,cells[a]));
			
		//}
	}
	
	cells[0].setAttribute('tabindex', 10000);    // or other number
	cells[0].focus();
	var img1=document.getElementById('imgeditar'+str);
	var img2=document.getElementById('imgsave'+str);
	var img3=document.getElementById('imgcancel'+str);

	img1.style.display="none";
	img2.style.display="block";
	img3.style.display="block";

	var tiempo=cells[2].innerText;
		cells[2].innerHTML="";
		cells[2].innerText="";
		cells[2].contentEditable="false";
		cells[2].innerHTML="<input type='time' id='tiempo2"+str+"' value='"+tiempo+"'/>";
	
	var activo=cells[4].innerText;
	cells[4].contentEditable="false";
	if(activo.localeCompare("Si")==0){
		var selectList = document.createElement("select");
   		selectList.id="optionactivo"+str;
 		var option = document.createElement("option");
    		 option.value="1"
    		 option.text = "Si"
    		 selectList.appendChild(option);
 		var option = document.createElement("option");
    		 option.value="0"
    		 option.text = "No"
    		 selectList.appendChild(option);
		
		cells[4].innerHTML="";
		cells[4].innerText="";
		cells[4].appendChild(selectList);
	}else if(activo.localeCompare("No")==0){
		
		var selectList = document.createElement("select");
   		selectList.id="optionactivo"+str;
		var option = document.createElement("option");
    		 option.value="0"
    		 option.text = "No"
    		 selectList.appendChild(option);
 		var option = document.createElement("option");
    		 option.value="1"
    		 option.text = "Si"
    		 selectList.appendChild(option);
 		  		
		cells[4].innerHTML="";
		cells[4].innerText="";
		cells[4].appendChild(selectList);
	}
	getdoctors();
}




function deleterecord(str){
	var res=confirm("�Seguro que deseas eliminar este registro?");

	if(res){
		REQUEST('appControl', 'deleteproceso', [str] , onprocesodelete, onerror);
	}
}

function onprocesodelete(dataResult){
	if(dataResult[0]['exito'].localeCompare("si")==0)
	{	getprocess();
		alert("Exito al eliminar los datos");
			
	}
	else if(dataResult[0]['exito'].localeCompare("sin")==0)
	{
		alert("El proceso no ha sido dado encontrado");
	}else
	{
		alert("Error al eliminar los datos. Intente nuevamente");
	}	
}


function myDeleteFunction() {
	var tableRef = document.getElementById('resultado');
	//var rowCount = tableRef.rows.length;
	
	var row = document.getElementsByTagName('tbody')[0];
	var rowCount = row.rows.length;

		for(var a=0;a<rowCount;a++)
		{

			//tableRef.deleteRow(1);
			row.deleteRow(0);
		}
    
}

function getval(grupo){
	var selec;

	for(var i = 0; i < grupo.length; i++) {
 	  if(grupo[i].checked)
    	   selec = grupo[i].value;
 	}
return selec;
}

function doSearch()
		{
			//var tableReg = document.getElementById('resultado');
			var tableReg = document.getElementsByTagName('tbody')[0];

			var searchText = document.getElementById('txtbuscar').value.toLowerCase();
			var cellsOfRow="";
			var found=false;
			var compareWith="";
 
			// Recorremos todas las filas con contenido de la tabla
			for (var i = 0; i < tableReg.rows.length; i++)
			{
				cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
				found = false;
				// Recorremos todas las celdas
				for (var j = 0; j < cellsOfRow.length && !found; j++)
				{
					compareWith = cellsOfRow[j].innerHTML.toLowerCase();
					// Buscamos el texto en el contenido de la celda
					if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1))
					{
						found = true;
					}
				}
				if(found)
				{
					tableReg.rows[i].style.display = '';
				} else {
					// si no ha encontrado ninguna coincidencia, esconde la
					// fila de la tabla
					tableReg.rows[i].style.display = 'none';
				}
			}
		}
function isNumber( input ) {
    return !isNaN( input );
}
