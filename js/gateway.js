﻿var gateway = 'bos.php';
var maxTries = 5;
var MessageError = new Array();
var strLang = 'es-MX';

var MessageError = Array();
MessageError['es-MX'] = Array();
MessageError['es-MX'][404] = 'Gateway no encontrado. No existe o está fuera de servicio.';
MessageError['es-MX'][408] = 'El servidor está muy ocupado. Intente más tarde.';
MessageError['es-MX'][500] = 'El servidor generó un error interno.';
MessageError['es-MX'][-10] = 'Resultado no está en formato JSON.';
MessageError['es-MX'][-10] = 'Resultado no está en formato JSON.';
MessageError['es-MX'][-11] = 'El resultado no es un objeto Response. Sólo se admiten objetos Response';
MessageError['es-MX'][-12] = 'El servidor generó un error mientras ejecutaba la consulta.';

function NuevoAjax()
{
	var xmlhttp=false;
    try
	{
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	}
	catch(e)
	{
		try
		{
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch(E)
		{
			xmlhttp =  false;
		}
	}
	
	if(!xmlhttp && typeof XMLHttpRequest!='undefined')
	{
		xmlhttp = new XMLHttpRequest();
	}
	return xmlhttp;
}

var gcls = '';
var gfnc = '';
var gparams;
var gonresult='';
var gonerror;
function REQUEST(cls, fnc, params, onresult, onerror)
{
	var method = 'POST', url = gateway;
	
	gcls = cls;
	gfnc = fnc;
	gonresult = onresult;
	gonerror = onerror;
	
	ajax = NuevoAjax();
    ajax.open(method, url, true); 
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==1)
		{
			
		}
		else if(ajax.readyState==4)
		{
			if(ajax.status==200)
			{
				var strResult = ajax.responseText;
				//console.log(strResult);
			//alert(strResult);
				try
				{
					var objResult = JSON.parse(strResult);
				}
				catch(e)
				{
					var errCode = -10;
					onerror({Code: errCode, Message: MessageError[strLang][errCode], Detail: e + ':\n' + ajax.responseText});
					//console.log(ajax.responseText);
				}
				
				if(typeof objResult['Success'] === 'undefined' || typeof objResult['Result'] === 'undefined' || typeof objResult['Error'] === 'undefined')
				{
					var errCode = -11;
					onerror({Code: errCode, Message: MessageError[strLang][errCode], Detail: ajax.responseText});
				}
				else
				{
					if(objResult['Success'] == false)
					{
						var errCode = -12;
						onerror({Code: errCode, Message: MessageError[strLang][errCode], Detail: objResult['Error']});
					}
					else
					{
						onresult(objResult['Result']);
					}
				}
			}
		}
		else if(ajax.status==408)
		{
			curTry++;
			if(curTry>=maxTries)
			{
				onerror({Code: ajax.status, Message: MessageError[strLang][ajax.status], Detail: ajax.responseText});
			}
			else
			{
				REQUEST(cls, fnc, params, onresult, onerror);
			}
		}
		else if(ajax.status>=400)
		{
			onerror({Code: ajax.status, Message: MessageError[strLang][ajax.status], Detail: ajax.responseText});
		}
	}
	ajax.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	//ajax.timeout = 5000;
//	ajax.ontimeout = ajaxTimeOut;
	ajax.send(JSON.stringify({Class: cls, Function: fnc, Params: params}));
	//var xmlHttpTimeout=setTimeout("ajaxTimeout()",30000);
}
/*
var curTry = 0;
var ajaxTimeout = function(){
   ajax.abort();
   curTry++;
	if(curTry>=maxTries)
	{
		gonerror({Code: 0, Message: 'TIMEOUT', Detail: 'El servicio ' + gfnc + ' esta tardando mucho en responder'});
	}
	else
	{
		REQUEST(gcls, gfnc, gparams, gonresult, gonerror);
	}

}*/